#include "TSHooks.hpp"

ADDR WinConsole__processJzLoc;

BlFunctionDef(void, , WinConsole__process);
BlFunctionHookDef(WinConsole__process);

/*
Letting BL relentlessly call ReadConsoleInput leads to bad performance.
As a fix, we can hook the process function and wait for an Input Event.
*/
void WinConsole__processHook()
{
	static HANDLE stdIn = GetStdHandle(STD_INPUT_HANDLE);

	if (WaitForSingleObject(stdIn, 0) == WAIT_OBJECT_0)
		WinConsole__processOriginal();
}

bool init()
{
	BlInit;

	BlScanFunctionHex(WinConsole__process, "81 EC ? ? ? ? A1 ? ? ? ? 33 C4 89 84 24 ? ? ? ? 53 55 56 57 8B 3D");
	BlCreateHook(WinConsole__process);
	BlTestEnableHook(WinConsole__process);

	// nop out if(numEvents)
	BlScanHex(WinConsole__processJzLoc, "0F 84 ? ? ? ? 8D 44 24 1C 33 F6 50 6A 14 8D 44 24 28 50 FF 77 08 FF 15");
	BlPatchBytes(6, WinConsole__processJzLoc, "\x90\x90\x90\x90\x90\x90");

	BlPrintf("%s (v%s-%s): init'd", PROJECT_NAME, PROJECT_VERSION, TSFUNCS_DEBUG ? "debug":"release");
	return true;
}

bool deinit()
{
	BlTestDisableHook(WinConsole__process);

	BlPrintf("%s: deinit'd", PROJECT_NAME);
	return true;
}

bool __stdcall DllMain(HINSTANCE hinstance, unsigned int reason, void *reserved)
{
	switch (reason) {
		case DLL_PROCESS_ATTACH:
			return init();
		case DLL_PROCESS_DETACH:
			return deinit();
		default:
			return true;
	}
}

extern "C" void __declspec(dllexport) __cdecl PROJECT_EXPORT(){}
