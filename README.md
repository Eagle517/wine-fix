# Wine Fix
This DLL patches out the resulting check of GetNumberOfConsoleInputEvents so Blockland accepts input from Wine.  This is for Wine 5.18+, as Wine 5.17- contains the curses backend with Wineconsole which works fine.

### WARNING: Wine 9.0 does not capture Blockland's console output at all, due to some breaking change.  I confirmed Wine 8.0.2 works.

## Usage
Use the latest version of [RedBlocklandLoader](https://gitlab.com/Eagle517/redblocklandloader/-/releases) to easily load the DLL into the game.

The DLL will automatically patch the game when it is loaded.

When running a Blockland dedicated server, you will typically want to rely on `xvfb` for a virtual display and `screen` or `tmux` to hop in and out of Blockland's console for management.
```
export WINEARCH=win32 # Ensure a 32bit prefix
export WINEDEBUG=-all # Prevent Wine errors/warnings from showing up in the console
unset DISPLAY # Prevent Wine from trying to use :0 and complaining about not having a display
screen -dmS "Blockland" xvfb-run -a wine Blockland.exe ptlaaxobimwroe -dedicated ...
```

## Building
This DLL relies on [TSFuncs](https://gitlab.com/Eagle517/tsfuncs) which you will need to build first.  Building with CMake is straightforward, although it has only been setup for MinGW.  A toolchain has been included for cross-compiling on Linux.
